<%@page import="entity.DanhSachThuThue"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.List" %>
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
   </head>
   <body>
      <h1>Xuất Excel</h1>
      <table cellpadding="1"  cellspacing="1" border="1" bordercolor="gray">
         <tr>
             <td style="text-align:center">STT</td>
             <td style="text-align:center">Số thuế</td>
             <td style="text-align:center">Họ tên</td>
             <td style="text-align:center">Số CMND/CCCD</td>
             <td style="text-align:center">Thông tin</td>
             <td style="text-align:center">Địa chỉ</td>
             <td style="text-align:center">Thời gian đóng thuê</td>
             <td style="text-align:center">Thu nhập tính thuế</td>
             <td style="text-align:center">Số tiền đóng thuê</td>
             <td style="text-align:center">Bậc thuế</td>
             <td style="text-align:center">Tháng đóng</td>
             <td style="text-align:center">Năm đóng</td>
             <td style="text-align:center">Trạng thái</td>
             <td style="text-align:center">Khu vực</td>
         </tr>
         <%
            List<DanhSachThuThue> lst  = (List<DanhSachThuThue>)request.getAttribute("DSExcel");
                  if (lst != null) {
                      response.setContentType("application/vnd.ms-excel");
                      response.setHeader("Content-Disposition", "inline; filename="+ "Danhsach.xls");
                  }
            int i = 1;
            for(DanhSachThuThue e: lst){
            %>
         <tr>
             <td><%= i++%></td>
             <td><%=e.getSoThue()%></td>
             <td><%=e.getHoTen()%></td>
             <td><%=e.getSoCMND()%></td>
             <td><%=e.getThongTin()%></td>
             <td><%=e.getDiaChi()%></td>
             <td><%=e.getThoiGianDongThue()%></td>
             <td><%=e.getThuNhapTinhThue()%></td>
             <td><%=e.getSoTienDongThue()%></td>
             <td><%=e.getBacThue()%></td>
             <td><%=e.getThangDong()%></td>
             <td><%=e.getNamDong()%></td>
             <td><%=e.getTrangThai()%></td>
             <td><%=e.getKhuVuc()%></td>
         </tr>
         <% 
             }
i=1;
            %>
      </table>
   </body>
</html>