/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import context.ConnectDB;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author Tran Tu
 */
public class RollBackDB {

    ConnectDB db = new ConnectDB();
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public void rollBackCauHinhThue() {
        String query = "Truncate Table CauHinhThue; INSERT [dbo].[CauHinhThue]  VALUES (1, N'Đến 5 triệu đồng', 5, N'0 triệu đồng + 5% TNTT', N'5% TNTT');\n" +
"INSERT [dbo].[CauHinhThue]  VALUES (2, N'Trên 05 trđ đến 10 trđ', 10, N'0,25 trđ + 10% TNTT trên 5 trđ', N'10% TNTT - 0,25 trđ');\n" +
"INSERT [dbo].[CauHinhThue]  VALUES (3, N'Trên 10 trđ đến 18 trđ', 15, N'0,75 trđ + 15% TNTT trên 10 trđ', N'15% TNTT - 0,75 trđ');\n" +
"INSERT [dbo].[CauHinhThue]  VALUES (4, N'Trên 18 trđ đến 32 trđ', 20, N'1,95 trđ + 20% TNTT trên 18 trđ', N'20% TNTT - 1,65 trđ');\n" +
"INSERT [dbo].[CauHinhThue]  VALUES (5, N'Trên 32 trđ đến 52 trđ', 25, N'4,75 trđ + 25% TNTT trên 32 trđ', N'25% TNTT - 3,25 trđ');\n" +
"INSERT [dbo].[CauHinhThue]  VALUES (6, N'Trên 52 trđ đến 80 trđ', 30, N'9,75 trđ + 30% TNTT trên 52 trđ', N'30 % TNTT - 5,85 trđ');\n" +
"INSERT [dbo].[CauHinhThue]  VALUES (7, N'Trên 80 trđ', 35, N'18,15 trđ + 35% TNTT trên 80 trđ', N'35% TNTT - 9,85 trđ');";
        try {
            conn = db.getConnection();//mo ket noi voi sql
            ps = conn.prepareStatement(query);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }
}
