/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import context.ConnectDB;
import entity.DanhSachThuThue;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tran Tu
 */
public class DanhSachThuThueDAO {
    ConnectDB db = new ConnectDB();
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    
    public List<DanhSachThuThue> getAllDanhSachThue() {
        List<DanhSachThuThue> lst = new ArrayList<>();
        String query = "select * from DanhSachThuThue";
        try {
            conn = db.getConnection();//ket noi
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                lst.add(new DanhSachThuThue(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getString(9),
                        rs.getString(10),
                        rs.getString(11),
                        rs.getString(12),
                        rs.getString(13),
                        rs.getString(14),
                        rs.getString(15),
                        rs.getString(16),
                        rs.getString(17)));
            }
        } catch (Exception e) {
        }

        return lst;
    }

    public List<DanhSachThuThue> searchListDanhSachThue(String type, String khuvuc, String startAt, String startEnd) {
        List<DanhSachThuThue> lst = new ArrayList<>();
        String query = "SELECT * FROM DanhSachThuThue Where TrangThai = ? "
                + "and ThoiGianDongThue > ? and ThoiGianDongThue < ?";
        String query2 = " and KhuVuc = ";
        try {
            conn = db.getConnection();//ket noi
            if(!khuvuc.equals("0")){
                query=query+query2+khuvuc;
            }
            ps = conn.prepareStatement(query);
            ps.setString(1, type);
            ps.setString(2, startAt);
            ps.setString(3, startEnd);
            rs = ps.executeQuery();
            while (rs.next()) {
                lst.add(new DanhSachThuThue(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getString(9),
                        rs.getString(10),
                        rs.getString(11),
                        rs.getString(12),
                        rs.getString(13),
                        rs.getString(14),
                        rs.getString(15),
                        rs.getString(16),
                        rs.getString(17)));
            }
        } catch (Exception e) {
        }

        return lst;
    }
    
    public List<DanhSachThuThue> searchListDanhSachThueDaDong(String ddlKhuVuc,String ddlThang,String ddlNam,String type) {
        List<DanhSachThuThue> lst = new ArrayList<>();
        String query = "SELECT * FROM DanhSachThuThue Where TrangThai = ? and KhuVuc = ? "
                + "and ThangDong = ? and NamDong = ?";
        try {
            conn = db.getConnection();//ket noi
            ps = conn.prepareStatement(query);
             ps.setString(1, type);
            ps.setString(2, ddlKhuVuc);
            ps.setString(3, ddlThang);
            ps.setString(4, ddlNam);
            rs = ps.executeQuery();
            while (rs.next()) {
                lst.add(new DanhSachThuThue(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getString(9),
                        rs.getString(10),
                        rs.getString(11),
                        rs.getString(12),
                        rs.getString(13),
                        rs.getString(14),
                        rs.getString(15),
                        rs.getString(16),
                        rs.getString(17)));
            }
        } catch (Exception e) {
        }
        return lst;
    }
    public static void main(String[] args) {
        System.out.println(new DanhSachThuThueDAO().searchListDanhSachThue("3", "1", "2021-06-05", "2021-06-06").size());
    }
    
}
