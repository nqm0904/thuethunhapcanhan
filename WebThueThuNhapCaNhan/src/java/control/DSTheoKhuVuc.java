/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import dao.DanhSachThuThueDAO;
import dao.TinhThanhDAO;
import entity.DanhSachThuThue;
import entity.TinhThanh;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Tran Tu
 */
@WebServlet(name = "DSTheoKhuVuc", urlPatterns = {"/dstheokhuvuc"})
public class DSTheoKhuVuc extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        TinhThanhDAO dao = new TinhThanhDAO();
        List<TinhThanh> lstTP = dao.getAllTinhThanh();
        request.setAttribute("lstTP", lstTP);
        request.getRequestDispatcher("DSTheoKhuVuc.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        DanhSachThuThueDAO dao = new DanhSachThuThueDAO();
        HttpSession session = request.getSession();
        List<DanhSachThuThue> lstDS = (List<DanhSachThuThue>) session.getAttribute("DSTheoKhuVuc");
        request.setAttribute("DSTheoKhuVuc", lstDS);
        RequestDispatcher rd = request.getRequestDispatcher("ExcelTheoKhuVuc.jsp");
        rd.forward(request, response);
    }

}
