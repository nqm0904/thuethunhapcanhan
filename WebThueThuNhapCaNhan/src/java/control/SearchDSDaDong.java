/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import dao.DanhSachThuThueDAO;
import entity.DanhSachThuThue;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Tran Tu
 */
@WebServlet(name = "SearchDSDaDong", urlPatterns = {"/SearchDSDaDong"})
public class SearchDSDaDong extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String type = request.getParameter("type");
        String ddlKhuVuc = request.getParameter("ddlKhuVuc");
        String ddlThang = request.getParameter("ddlThang");
        String ddlNam = request.getParameter("ddlNam");
//        String Kieu = request.getParameter("Kieu");
        DanhSachThuThueDAO db = new DanhSachThuThueDAO();
        List<DanhSachThuThue> lstDS = db.searchListDanhSachThueDaDong(ddlKhuVuc, ddlThang, ddlNam, type);
        int dem = 1;
        PrintWriter out = response.getWriter();
        if (lstDS.size() > 0) {
            request.setAttribute("lstDS123", lstDS);
            HttpSession session = request.getSession();
            session.setAttribute("DSExcel", lstDS);
            for (DanhSachThuThue item : lstDS) {
                out.print("<tr>\n"
                        + "                                                                    <td>\n" + dem++ + "</td>\n"
                        + "                                                                    <td>" + item.getSoThue() + "</td>\n"
                        + "                                                                    <td>" + item.getHoTen() + "</td>\n"
                        + "                                                                    <td>" + item.getSoCMND() + "</td>\n"
                        + "                                                                    <td>" + item.getThongTin() + "</td>\n"
                        + "                                                                    <td>" + item.getDiaChi() + "</td>\n"
                        + "                                                                    <td>" + item.getThoiGianDongThue() + "</td>\n"
                        + "                                                                    <td style=\"text-align: right\">" + item.getThuNhapTinhThue() + "</td>\n"
                        + "                                                                    <td style=\"text-align: right\">" + item.getSoTienDongThue() + "</td>\n"
                        + "                                                                    <td>" + item.getBacThue() + "</td>\n"
                        + "                                                                    <td>" + item.getThangDong() + "</td>\n"
                        + "                                                                    <td>" + item.getNamDong() + "</td>\n"
                        + "                                                                    <td>" + item.getTenTrangThai() + "</td>\n"
                        + "                                                                    <td>" + item.getTenKhuVuc() + "</td>\n"
                        + "                                                                </tr>");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
