/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import dao.DanhSachThuThueDAO;
import dao.TinhThanhDAO;
import entity.DanhSachThuThue;
import entity.TinhThanh;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Hp
 */
@WebServlet(name = "ExportExcel", urlPatterns = {"/ExportExcel"})
public class ExportExcel extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        request.setCharacterEncoding("UTF-8");
//        DanhSachThuThueDAO dao = new DanhSachThuThueDAO();
//        TinhThanhDAO dao1 = new TinhThanhDAO();
//        List<DanhSachThuThue> lstDS = dao.getAllDanhSachThue();
//        List<TinhThanh> lstTP = dao1.getAllTinhThanh();
//        //b2 set data jsp
//        request.setAttribute("lstDS123", lstDS);
//        request.setAttribute("lstTP", lstTP);
//        HttpSession session = request.getSession();
//        session.setAttribute("DSExcel", lstDS);
//        RequestDispatcher rd = request.getRequestDispatcher("XuatExcel.jsp");
//        rd.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        DanhSachThuThueDAO dao = new DanhSachThuThueDAO();
        HttpSession session = request.getSession();
        List<DanhSachThuThue> lstDS = (List<DanhSachThuThue>) session.getAttribute("DSExcel");
        request.setAttribute("DSExcel", lstDS);
        RequestDispatcher rd = request.getRequestDispatcher("excelreport.jsp");
        rd.forward(request, response);
    }

}
