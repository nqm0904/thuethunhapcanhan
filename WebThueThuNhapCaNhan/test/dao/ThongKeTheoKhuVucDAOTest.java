/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.ThongKeTheoKhuVuc;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tran Tu
 */
public class ThongKeTheoKhuVucDAOTest {
    
    public ThongKeTheoKhuVucDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetAllThongKeTheoKhuVuc() {
        System.out.println("getAllThongKeTheoKhuVuc");
        ThongKeTheoKhuVucDAO instance = new ThongKeTheoKhuVucDAO();
        int expResult = 315;
        List<ThongKeTheoKhuVuc> result = instance.getAllThongKeTheoKhuVuc();
        assertEquals(expResult, result.size());
    }

    @Test
    public void testGetAllThongKeTheoKhuVucByParam() {
        System.out.println("getAllThongKeTheoKhuVucByParam");
        String khuvuc = "1";
        String thang = "0";
        String nam = "0";
        ThongKeTheoKhuVucDAO instance = new ThongKeTheoKhuVucDAO();
        int expResult = 5;
        List<ThongKeTheoKhuVuc> result = instance.getAllThongKeTheoKhuVuc(khuvuc, thang, nam);
        assertEquals(expResult, result.size());
    }
     @Test
    public void testGetAllThongKeTheoKhuVucByParam2() {
        System.out.println("getAllThongKeTheoKhuVucByParam");
        String khuvuc = "0";
        String thang = "5";
        String nam = "2021";
        ThongKeTheoKhuVucDAO instance = new ThongKeTheoKhuVucDAO();
        int expResult = 63;
        List<ThongKeTheoKhuVuc> result = instance.getAllThongKeTheoKhuVuc(khuvuc, thang, nam);
        assertEquals(expResult, result.size());
    }
    @Test
    public void testGetAllThongKeTheoKhuVucByParam3() {
        System.out.println("getAllThongKeTheoKhuVucByParam");
        String khuvuc = "1";
        String thang = "5";
        String nam = "2021";
        ThongKeTheoKhuVucDAO instance = new ThongKeTheoKhuVucDAO();
        int expResult = 1;
        List<ThongKeTheoKhuVuc> result = instance.getAllThongKeTheoKhuVuc(khuvuc, thang, nam);
        assertEquals(expResult, result.size());
    }
    
}
