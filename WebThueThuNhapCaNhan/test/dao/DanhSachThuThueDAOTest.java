/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entity.DanhSachThuThue;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Tran Tu
 */
public class DanhSachThuThueDAOTest {
    
    public DanhSachThuThueDAOTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetAllDanhSachThue() {
        System.out.println("getAllDanhSachThue");
        DanhSachThuThueDAO instance = new DanhSachThuThueDAO();
        int expResult = 7;
        List<DanhSachThuThue> result = instance.getAllDanhSachThue();
        assertEquals(expResult, result.size());
    }

    @Test
    public void testSearchListDanhSachThue() {
        System.out.println("searchListDanhSachThue");
        String type = "1";
        String khuvuc = "1";
        String startAt = "2021-04-02";
        String startEnd = "2021-06-02";
        DanhSachThuThueDAO instance = new DanhSachThuThueDAO();
        int expResult = 2;
        List<DanhSachThuThue> result = instance.searchListDanhSachThue(type, khuvuc, startAt, startEnd);
        assertEquals(expResult, result.size());
    }
    @Test
    public void testSearchListDanhSachThueKhongCo() {
        System.out.println("searchListDanhSachThue");
        String type = "2";
        String khuvuc = "1";
        String startAt = "2021-04-02";
        String startEnd = "2021-06-02";
        DanhSachThuThueDAO instance = new DanhSachThuThueDAO();
        int expResult = 0;
        List<DanhSachThuThue> result = instance.searchListDanhSachThue(type, khuvuc, startAt, startEnd);
        assertEquals(expResult, result.size());
    }

    @Test
    public void testSearchListDanhSachThueDaDong() {
        System.out.println("searchListDanhSachThueDaDong");
        String ddlKhuVuc = "1";
        String ddlThang = "5";
        String ddlNam = "2021";
        String type = "1";
        DanhSachThuThueDAO instance = new DanhSachThuThueDAO();
        int expResult = 2;
        List<DanhSachThuThue> result = instance.searchListDanhSachThueDaDong(ddlKhuVuc, ddlThang, ddlNam, type);
        assertEquals(expResult, result.size());
    }
     @Test
    public void testSearchListDanhSachThueDaDongKhongCo() {
        System.out.println("searchListDanhSachThueDaDong");
        String ddlKhuVuc = "2";
        String ddlThang = "5";
        String ddlNam = "2021";
        String type = "1";
        DanhSachThuThueDAO instance = new DanhSachThuThueDAO();
        int expResult = 0;
        List<DanhSachThuThue> result = instance.searchListDanhSachThueDaDong(ddlKhuVuc, ddlThang, ddlNam, type);
        assertEquals(expResult, result.size());
    }
    @Test
    public void testSearchListDanhSachThueChuaDong() {
        System.out.println("searchListDanhSachThueDaDong");
        String ddlKhuVuc = "1";
        String ddlThang = "6";
        String ddlNam = "2021";
        String type = "2";
        DanhSachThuThueDAO instance = new DanhSachThuThueDAO();
        int expResult = 1;
        List<DanhSachThuThue> result = instance.searchListDanhSachThueDaDong(ddlKhuVuc, ddlThang, ddlNam, type);
        assertEquals(expResult, result.size());
    }
      @Test
    public void testSearchListDanhSachThueChuaDongKhongCo() {
        System.out.println("searchListDanhSachThueDaDong");
        String ddlKhuVuc = "2";
        String ddlThang = "6";
        String ddlNam = "2021";
        String type = "2";
        DanhSachThuThueDAO instance = new DanhSachThuThueDAO();
        int expResult = 0;
        List<DanhSachThuThue> result = instance.searchListDanhSachThueDaDong(ddlKhuVuc, ddlThang, ddlNam, type);
        assertEquals(expResult, result.size());
    }
    
}
